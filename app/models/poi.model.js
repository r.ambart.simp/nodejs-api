const sql = require("./db.js");

// constructor
const Poi = function(poi) {
  this.idPOI = poi.idPOI;
  this.poi_name = poi.poi_name;
  this.poi_email = poi.poi_email;
  this.poi_geo = poi.poi_geo;
  this.poi_hidden = poi.poi_hidden;
};


Poi.create = (newPoi, result) => {
  sql.query("INSERT INTO poi SET ?", newPoi, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created Poi: ", { id: res.insertId, ...newPoi });
    result(null, { id: res.insertId, ...newPoi });
  });
};

Poi.findById = (idPOI, result) => {
  sql.query(`SELECT * FROM poi WHERE idPOI = ${idPOI}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found Poi: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Poi with the id
    result({ kind: "not_found" }, null);
  });
};

Poi.getAll = result => {
  sql.query("SELECT * FROM poi", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Pois: ", res);
    result(null, res);
  });
};

Poi.updateById = (idPOI, Poi, result) => {
  sql.query(
    "UPDATE poi SET POI_EMAIL = ?, POI_NAME = ? WHERE idPOI = ?",
    [Poi.poi_email, Poi.poi_name, idPOI],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Poi with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated Poi: ", { idPOI: idPOI, ...Poi });
      result(null, { idPOI: idPOI, ...Poi });
    }
  );
};

Poi.remove = (idPOI, result) => {
  sql.query("DELETE FROM poi WHERE idPOI = ?", idPOI, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Poi with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Poi with id: ", idPOI);
    result(null, res);
  });
};

Poi.removeAll = result => {
  sql.query("DELETE FROM poi", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} Pois`);
    result(null, res);
  });
};

module.exports = Poi;
